from django.urls import path
from .views import (
    home,
    ContactListView,
    ContactCreateView,
    ContactDetailView,
    ContactUpdateView,
    ContactDeleteView
)

urlpatterns = [
    path('', home, name='home'),
    path('list/', ContactListView.as_view(), name='directory-list'),
    path('add/', ContactCreateView.as_view(), name='directory-create'),
    path('contact/<int:pk>/', ContactDetailView.as_view(), name='contact-detail'),
    path('contact/<int:pk>/update/', ContactUpdateView.as_view(), name='contact-update'),
    path('contact/<int:pk>/delete/', ContactDeleteView.as_view(), name='contact-delete'),
]
